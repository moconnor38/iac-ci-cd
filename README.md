
GitLab CI/CD pipeline to test, build and deploy a containerised Django/PostgreSQL [app](https://docs.djangoproject.com/en/4.0/intro/tutorial01/) to an AWS environment. Terraform used to provision AWS resources.

Infrastructure consists of an ELB, an EC2 bastion server, a VPC with an ECS cluster containing an nginx proxy to serve static content, the Django app and database instances
Docker images are pushed to and stored in an ECR once built
Terraform state is stored in an S3 bucket and the state lock mechanism in a DynamoDB table
Pipeline is triggered on merge to the main branch
