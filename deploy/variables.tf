variable "prefix" {
  default = "iac"
}

variable "project" {
  default = "iac-app"
}

variable "db_username" {
  description = "Username for RDS postgres instance"
}

variable "db_password" {
  description = "Password for RDS postgres instance"
}

variable "bastion_key_name" {
  default = "iac-app-bastion"
}

variable "ecr_image_iac" {
  description = "ECR image for iac app"
  default     = "965051556300.dkr.ecr.eu-west-1.amazonaws.com/iac-app:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "965051556300.dkr.ecr.eu-west-1.amazonaws.com/iac-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
