output "db_host" {
  value = aws_db_instance.main.address
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "iac_endpoint" {
  value = aws_lb.iac.dns_name
}
