terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

terraform {
  backend "s3" {
    bucket         = "iac-app-tfstate"
    key            = "iac-app.tfstate"
    region         = "eu-west-1"
    encrypt        = true
    dynamodb_table = "iac-app-tfstate-lock"
  }
}

provider "aws" {
  region = "eu-west-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
